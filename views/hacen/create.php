<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Hacen */

$this->title = 'Create Hacen';
$this->params['breadcrumbs'][] = ['label' => 'Hacens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hacen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
