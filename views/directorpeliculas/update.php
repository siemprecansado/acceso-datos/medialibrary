<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directorpeliculas */

$this->title = 'Update Directorpeliculas: ' . $model->iddirector;
$this->params['breadcrumbs'][] = ['label' => 'Directorpeliculas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddirector, 'url' => ['view', 'id' => $model->iddirector]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="directorpeliculas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
