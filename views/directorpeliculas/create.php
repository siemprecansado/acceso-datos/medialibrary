<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Directorpeliculas */

$this->title = 'Create Directorpeliculas';
$this->params['breadcrumbs'][] = ['label' => 'Directorpeliculas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directorpeliculas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
