<?php

namespace app\controllers;

use Yii;
use app\models\Guardan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GuardanController implements the CRUD actions for Guardan model.
 */
class GuardanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Guardan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Guardan::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Guardan model.
     * @param string $idusuario
     * @param string $idmedia
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idusuario, $idmedia)
    {
        return $this->render('view', [
            'model' => $this->findModel($idusuario, $idmedia),
        ]);
    }

    /**
     * Creates a new Guardan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Guardan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idusuario' => $model->idusuario, 'idmedia' => $model->idmedia]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Guardan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $idusuario
     * @param string $idmedia
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idusuario, $idmedia)
    {
        $model = $this->findModel($idusuario, $idmedia);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idusuario' => $model->idusuario, 'idmedia' => $model->idmedia]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Guardan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $idusuario
     * @param string $idmedia
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idusuario, $idmedia)
    {
        $this->findModel($idusuario, $idmedia)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Guardan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $idusuario
     * @param string $idmedia
     * @return Guardan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idusuario, $idmedia)
    {
        if (($model = Guardan::findOne(['idusuario' => $idusuario, 'idmedia' => $idmedia])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
