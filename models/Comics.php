<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comics".
 *
 * @property string $idcomic
 * @property int|null $num_entregas
 * @property int|null $num_reboots
 */
class Comics extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comics';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcomic'], 'required'],
            [['num_entregas', 'num_reboots'], 'integer'],
            [['idcomic'], 'string', 'max' => 20],
            [['idcomic'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcomic' => 'Idcomic',
            'num_entregas' => 'Num Entregas',
            'num_reboots' => 'Num Reboots',
        ];
    }
}
