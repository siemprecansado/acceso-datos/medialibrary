<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "miembro".
 *
 * @property string $idmiembro
 * @property string|null $miembro
 */
class Miembro extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'miembro';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idmiembro'], 'required'],
            [['idmiembro', 'miembro'], 'string', 'max' => 20],
            [['idmiembro'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmiembro' => 'Idmiembro',
            'miembro' => 'Miembro',
        ];
    }
}
