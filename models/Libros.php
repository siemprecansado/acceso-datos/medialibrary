<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "libros".
 *
 * @property string $idlibro
 * @property string|null $editorial
 * @property int|null $num_ediciones
 */
class Libros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'libros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idlibro'], 'required'],
            [['num_ediciones'], 'integer'],
            [['idlibro', 'editorial'], 'string', 'max' => 20],
            [['idlibro'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idlibro' => 'Idlibro',
            'editorial' => 'Editorial',
            'num_ediciones' => 'Num Ediciones',
        ];
    }
}
