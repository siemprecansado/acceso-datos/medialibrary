<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peliculas".
 *
 * @property string $idpelicula
 * @property string|null $productora
 */
class Peliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpelicula'], 'required'],
            [['idpelicula', 'productora'], 'string', 'max' => 20],
            [['idpelicula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpelicula' => 'Idpelicula',
            'productora' => 'Productora',
        ];
    }
}
