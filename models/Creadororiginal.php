<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "creadororiginal".
 *
 * @property string $idcreador
 * @property string|null $creador_original
 */
class Creadororiginal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'creadororiginal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcreador'], 'required'],
            [['idcreador', 'creador_original'], 'string', 'max' => 20],
            [['idcreador'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcreador' => 'Idcreador',
            'creador_original' => 'Creador Original',
        ];
    }
}
