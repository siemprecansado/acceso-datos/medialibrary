<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property string $idusuario
 *
 * @property Guardan[] $guardans
 * @property Media[] $idmedia
 * @property Hacen[] $hacens
 * @property Reviews[] $idreviews
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idusuario'], 'required'],
            [['idusuario'], 'string', 'max' => 20],
            [['idusuario'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idusuario' => 'Idusuario',
        ];
    }

    /**
     * Gets query for [[Guardans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGuardans()
    {
        return $this->hasMany(Guardan::className(), ['idusuario' => 'idusuario']);
    }

    /**
     * Gets query for [[Idmedia]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdmedia()
    {
        return $this->hasMany(Media::className(), ['idmedia' => 'idmedia'])->viaTable('guardan', ['idusuario' => 'idusuario']);
    }

    /**
     * Gets query for [[Hacens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHacens()
    {
        return $this->hasMany(Hacen::className(), ['idusuario' => 'idusuario']);
    }

    /**
     * Gets query for [[Idreviews]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdreviews()
    {
        return $this->hasMany(Reviews::className(), ['idreview' => 'idreview'])->viaTable('hacen', ['idusuario' => 'idusuario']);
    }
}
