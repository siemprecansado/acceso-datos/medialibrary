<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "genero".
 *
 * @property string $idgenero
 * @property string|null $peliculas_codigo
 * @property string|null $genero
 */
class Genero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'genero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idgenero'], 'required'],
            [['idgenero', 'peliculas_codigo', 'genero'], 'string', 'max' => 20],
            [['idgenero'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idgenero' => 'Idgenero',
            'peliculas_codigo' => 'Peliculas Codigo',
            'genero' => 'Genero',
        ];
    }
}
