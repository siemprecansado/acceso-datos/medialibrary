<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autor".
 *
 * @property string $idautor
 * @property string|null $autor
 */
class Autor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idautor'], 'required'],
            [['idautor', 'autor'], 'string', 'max' => 20],
            [['idautor'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idautor' => 'Idautor',
            'autor' => 'Autor',
        ];
    }
}
