<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directorpeliculas".
 *
 * @property string $iddirector
 * @property string|null $director
 */
class Directorpeliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directorpeliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddirector'], 'required'],
            [['iddirector', 'director'], 'string', 'max' => 20],
            [['iddirector'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddirector' => 'Iddirector',
            'director' => 'Director',
        ];
    }
}
