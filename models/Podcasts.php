<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "podcasts".
 *
 * @property string $idpodcast
 * @property string|null $nombre
 * @property int|null $num_episodios
 */
class Podcasts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'podcasts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpodcast'], 'required'],
            [['num_episodios'], 'integer'],
            [['idpodcast', 'nombre'], 'string', 'max' => 20],
            [['idpodcast'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpodcast' => 'Idpodcast',
            'nombre' => 'Nombre',
            'num_episodios' => 'Num Episodios',
        ];
    }
}
