<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property string $idreview
 * @property string|null $contenido
 * @property float|null $puntuacion
 *
 * @property Hacen[] $hacens
 * @property Usuarios[] $idusuarios
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idreview'], 'required'],
            [['contenido'], 'string'],
            [['puntuacion'], 'number'],
            [['idreview'], 'string', 'max' => 20],
            [['idreview'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idreview' => 'Idreview',
            'contenido' => 'Contenido',
            'puntuacion' => 'Puntuacion',
        ];
    }

    /**
     * Gets query for [[Hacens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHacens()
    {
        return $this->hasMany(Hacen::className(), ['idreview' => 'idreview']);
    }

    /**
     * Gets query for [[Idusuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdusuarios()
    {
        return $this->hasMany(Usuarios::className(), ['idusuario' => 'idusuario'])->viaTable('hacen', ['idreview' => 'idreview']);
    }
}
