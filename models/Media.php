<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "media".
 *
 * @property string $idmedia
 * @property float|null $media_usuario
 * @property float|null $puntuacion
 * @property string|null $nombre
 * @property string|null $fecha_lanzamiento
 * @property float|null $puntuacion_criticos
 *
 * @property Guardan[] $guardans
 * @property Usuarios[] $idusuarios
 */
class Media extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'media';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idmedia'], 'required'],
            [['media_usuario', 'puntuacion', 'puntuacion_criticos'], 'number'],
            [['fecha_lanzamiento'], 'safe'],
            [['idmedia', 'nombre'], 'string', 'max' => 20],
            [['idmedia'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idmedia' => 'Idmedia',
            'media_usuario' => 'Media Usuario',
            'puntuacion' => 'Puntuacion',
            'nombre' => 'Nombre',
            'fecha_lanzamiento' => 'Fecha Lanzamiento',
            'puntuacion_criticos' => 'Puntuacion Criticos',
        ];
    }

    /**
     * Gets query for [[Guardans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGuardans()
    {
        return $this->hasMany(Guardan::className(), ['idmedia' => 'idmedia']);
    }

    /**
     * Gets query for [[Idusuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdusuarios()
    {
        return $this->hasMany(Usuarios::className(), ['idusuario' => 'idusuario'])->viaTable('guardan', ['idmedia' => 'idmedia']);
    }
}
