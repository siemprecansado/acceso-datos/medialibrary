<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directorseries".
 *
 * @property string $iddirectorserie
 * @property string|null $director
 */
class Directorseries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directorseries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddirectorserie'], 'required'],
            [['iddirectorserie', 'director'], 'string', 'max' => 20],
            [['iddirectorserie'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddirectorserie' => 'Iddirectorserie',
            'director' => 'Director',
        ];
    }
}
