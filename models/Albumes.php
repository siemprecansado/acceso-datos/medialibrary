<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "albumes".
 *
 * @property string $idalbum
 * @property string|null $grupo
 * @property string|null $discografica
 */
class Albumes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'albumes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idalbum'], 'required'],
            [['idalbum', 'grupo', 'discografica'], 'string', 'max' => 20],
            [['idalbum'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idalbum' => 'Idalbum',
            'grupo' => 'Grupo',
            'discografica' => 'Discografica',
        ];
    }
}
