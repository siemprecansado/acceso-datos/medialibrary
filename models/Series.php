<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "series".
 *
 * @property string $idserie
 * @property string|null $canal
 * @property int|null $temporadas
 * @property int|null $episodios
 */
class Series extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'series';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idserie'], 'required'],
            [['temporadas', 'episodios'], 'integer'],
            [['idserie', 'canal'], 'string', 'max' => 20],
            [['idserie'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idserie' => 'Idserie',
            'canal' => 'Canal',
            'temporadas' => 'Temporadas',
            'episodios' => 'Episodios',
        ];
    }
}
